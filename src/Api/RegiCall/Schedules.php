<?php

namespace Adsysco\RegiCareApiClient\Api\RegiCall;

use Adsysco\RegiCareApiClient\Api\HttpApi;
use Adsysco\RegiCareApiClient\Models\RegiCall\Schedule;

class Schedules extends HttpApi
{
	public function index($filters = [], $offset = 0, $limit = 10)
	{
		$response = $this->httpGet('api/v2/regicall/schedules', array_merge($filters, ['offset' => $offset, 'limit' => $limit]));

		return $this->hydrateResponseCollection($response, Schedule::class);
	}
}
