<?php

namespace Adsysco\RegiCareApiClient\Api\RegiCall;

use Adsysco\RegiCareApiClient\Api\HttpApi;
use Adsysco\RegiCareApiClient\Models\RegiCall\CallAttempt;

class CallAttempts extends HttpApi
{
	/**
	 * @param array $filters
	 * @param int   $offset
	 * @param int   $limit
	 *
	 * @return mixed|\Psr\Http\Message\ResponseInterface
	 * @throws \Exception
	 */
	public function index($filters = [], $offset = 0, $limit = 10)
	{
		$response = $this->httpGet('api/v2/regicall/callAttempts', array_merge($filters, ['offset' => $offset, 'limit' => $limit]));

		return $this->hydrateResponseCollection($response, CallAttempt::class);
	}

	/**
	 * @param $id
	 *
	 * @return mixed|\Psr\Http\Message\ResponseInterface
	 * @throws \Exception
	 */
	public function show($id)
	{
		$response = $this->httpGet('api/v2/regicall/callAttempts/' . $id);

		return $this->hydrateResponse($response, CallAttempt::class);
	}

	/**
	 * @param CallAttempt $callAttempt
	 *
	 * @return mixed|\Psr\Http\Message\ResponseInterface
	 * @throws \Exception
	 */
	public function create(CallAttempt $callAttempt)
	{
		$response = $this->httpPost(sprintf('api/v2/regicall/calls/%d/callAttempt', $callAttempt->call_id), $callAttempt->toArray());

		return $this->hydrateResponse($response, CallAttempt::class);
	}
}