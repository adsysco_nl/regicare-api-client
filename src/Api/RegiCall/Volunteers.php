<?php

namespace Adsysco\RegiCareApiClient\Api\RegiCall;

use Adsysco\RegiCareApiClient\Api\HttpApi;
use Adsysco\RegiCareApiClient\Models\RegiCall\Volunteer;

class Volunteers extends HttpApi
{
    public function index($filters = [], $offset = 0, $limit = 10)
    {
        $response = $this->httpGet('api/v2/regicall/volunteers', array_merge($filters, ['offset' => $offset, 'limit' => $limit]));

        return $this->hydrateResponseCollection($response, Volunteer::class);
    }

	public function show($id)
	{
		$response = $this->httpGet('api/v2/regicall/volunteers/' . $id);

		return $this->hydrateResponse($response, Volunteer::class);
	}
}
