<?php

namespace Adsysco\RegiCareApiClient\Api\RegiCall;

use Adsysco\RegiCareApiClient\Api\HttpApi;
use Adsysco\RegiCareApiClient\Models\Count;
use Adsysco\RegiCareApiClient\Models\RegiCall\Call;

class Calls extends HttpApi
{
    public function index($filters = [], $offset = 0, $limit = 10)
    {
        $response = $this->httpGet('api/v2/regicall/calls', array_merge($filters, ['offset' => $offset, 'limit' => $limit]));

        return $this->hydrateResponseCollection($response, Call::class);
    }

    public function show($id)
    {
        $response = $this->httpGet('api/v2/regicall/calls/' . $id);

        return $this->hydrateResponse($response, Call::class);
    }

    /**
     * @param $id
     * @param $data
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function update($id, $data)
    {
        $response = $this->httpPatch(sprintf('api/v2/regicall/calls/%d', $id), $data);

        return $this->hydrateResponse($response, Call::class);
    }

    /**
     * @param  array  $filters
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */

    public function count($filters = [])
    {
        $response = $this->httpGet('api/v2/regicall/calls/count', array_merge($filters));

        return $this->hydrateResponse($response, Count::class);
    }
}
