<?php

namespace Adsysco\RegiCareApiClient\Api\RegiCall;

use Adsysco\RegiCareApiClient\Api\HttpApi;
use Adsysco\RegiCareApiClient\Models\RegiCall\Period;

class Periods extends HttpApi
{
	public function index($filters = [], $offset = 0, $limit = 10)
	{
		$response = $this->httpGet('api/v2/regicall/periods', array_merge($filters, ['offset' => $offset, 'limit' => $limit]));

		return $this->hydrateResponseCollection($response, Period::class);
	}

    public function show($id)
    {
        $response = $this->httpGet('api/v2/regicall/periods/' . $id);

        return $this->hydrateResponse($response, Period::class);
    }
}
