<?php

namespace Adsysco\RegiCareApiClient\Api\RegiCall;

use Adsysco\RegiCareApiClient\Api\HttpApi;
use Adsysco\RegiCareApiClient\Models\RegiCall\Me as MeModel;

class Me extends HttpApi
{
    public function show()
    {
        $response = $this->httpGet('api/v2/regicall/me');

        return $this->hydrateResponse($response, MeModel::class);
    }
}
