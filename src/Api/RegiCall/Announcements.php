<?php

namespace Adsysco\RegiCareApiClient\Api\RegiCall;

use Adsysco\RegiCareApiClient\Api\HttpApi;
use Adsysco\RegiCareApiClient\Models\RegiCall\Announcement;

class Announcements extends HttpApi
{
	public function index($filters = [], $offset = 0, $limit = 10)
	{
		$response = $this->httpGet('api/v2/regicall/announcements', array_merge($filters, ['offset' => $offset, 'limit' => $limit]));

		return $this->hydrateResponseCollection($response, Announcement::class);
	}

	public function show($id)
	{
		$response = $this->httpGet('api/v2/regicall/announcements/' . $id);

		return $this->hydrateResponse($response, Announcement::class);
	}
}
