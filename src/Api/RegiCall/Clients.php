<?php

namespace Adsysco\RegiCareApiClient\Api\RegiCall;

use Adsysco\RegiCareApiClient\Api\HttpApi;
use Adsysco\RegiCareApiClient\Models\RegiCall\Client;

class Clients extends HttpApi
{
    public function index($filters = [], $offset = 0, $limit = 10)
    {
        $response = $this->httpGet('api/v2/regicall/clients', array_merge($filters, ['offset' => $offset, 'limit' => $limit]));

        return $this->hydrateResponseCollection($response, Client::class);
    }

	public function show($id)
	{
		$response = $this->httpGet('api/v2/regicall/clients/' . $id);

		return $this->hydrateResponse($response, Client::class);
	}
}
