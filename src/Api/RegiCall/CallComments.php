<?php

namespace Adsysco\RegiCareApiClient\Api\RegiCall;

use Adsysco\RegiCareApiClient\Api\HttpApi;
use Adsysco\RegiCareApiClient\Models\DeleteResponse;
use Adsysco\RegiCareApiClient\Models\RegiCall\CallComment;

class CallComments extends HttpApi
{
	public function index($filters = [], $offset = 0, $limit = 10)
	{
		$response = $this->httpGet('api/v2/regicall/callComments', array_merge($filters, ['offset' => $offset, 'limit' => $limit]));

		return $this->hydrateResponseCollection($response, CallComment::class);
	}

	public function show($id)
	{
		$response = $this->httpGet('api/v2/regicall/callComments/' . $id);

		return $this->hydrateResponse($response, CallComment::class);
	}

    /**
     * @param CallComment $callComment
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function create(CallComment $callComment)
    {
        $response = $this->httpPost(sprintf('api/v2/regicall/calls/%d/callComment', $callComment->call_id), $callComment->toArray());

        return $this->hydrateResponse($response, CallComment::class);
    }

    /**
     * @param $id
     * @param $data
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function update($id, $data)
    {
        $response = $this->httpPatch(sprintf('api/v2/regicall/callComments/%d', $id), $data);

        return $this->hydrateResponse($response, CallComment::class);
    }

    /**
     * @param $id
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function delete($id)
    {
        $response = $this->httpDelete(sprintf('api/v2/regicall/callComments/%d', $id));

        return $this->hydrateResponse($response, DeleteResponse::class);
    }
}
