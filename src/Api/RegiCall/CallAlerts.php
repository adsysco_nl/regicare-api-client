<?php

namespace Adsysco\RegiCareApiClient\Api\RegiCall;

use Adsysco\RegiCareApiClient\Api\HttpApi;
use Adsysco\RegiCareApiClient\Models\DeleteResponse;
use Adsysco\RegiCareApiClient\Models\RegiCall\CallAlert;

class CallAlerts extends HttpApi
{
	public function index($filters = [], $offset = 0, $limit = 10)
	{
		$response = $this->httpGet('api/v2/regicall/callAlerts', array_merge($filters, ['offset' => $offset, 'limit' => $limit]));

		return $this->hydrateResponseCollection($response, CallAlert::class);
	}

	public function show($id)
	{
		$response = $this->httpGet('api/v2/regicall/callAlerts/' . $id);

		return $this->hydrateResponse($response, CallAlert::class);
	}

    /**
     * @param CallAlert $callAlert
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function create(CallAlert $callAlert)
    {
        $response = $this->httpPost(sprintf('api/v2/regicall/calls/%d/callAlert', $callAlert->call_id), $callAlert->toArray());

        return $this->hydrateResponse($response, CallAlert::class);
    }

    /**
     * @param $id
     * @param $data
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function update($id, $data)
    {
        $response = $this->httpPatch(sprintf('api/v2/regicall/callAlerts/%d', $id), $data);

        return $this->hydrateResponse($response, CallAlert::class);
    }

    /**
     * @param $id
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function delete($id)
    {
        $response = $this->httpDelete(sprintf('api/v2/regicall/callAlerts/%d', $id));

        return $this->hydrateResponse($response, DeleteResponse::class);
    }
}
