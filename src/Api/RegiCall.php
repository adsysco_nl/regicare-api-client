<?php

namespace Adsysco\RegiCareApiClient\Api;

use Adsysco\RegiCareApiClient\Api\RegiCall\Announcements;
use Adsysco\RegiCareApiClient\Api\RegiCall\CallAlerts;
use Adsysco\RegiCareApiClient\Api\RegiCall\CallAttempts;
use Adsysco\RegiCareApiClient\Api\RegiCall\Clients;
use Adsysco\RegiCareApiClient\Api\RegiCall\CallComments;
use Adsysco\RegiCareApiClient\Api\RegiCall\Calls;
use Adsysco\RegiCareApiClient\Api\RegiCall\Volunteers;
use Adsysco\RegiCareApiClient\Api\RegiCall\Schedules;
use Adsysco\RegiCareApiClient\Api\RegiCall\Me;
use Adsysco\RegiCareApiClient\Api\RegiCall\Periods;

class RegiCall extends HttpApi
{
	public function announcements(): Announcements
	{
		return new Announcements($this->httpClient, $this->requestBuilder, $this->hydrator);
	}

	public function callAttempts(): CallAttempts
	{
		return new CallAttempts($this->httpClient, $this->requestBuilder, $this->hydrator);
	}

	public function clients(): Clients
	{
		return new Clients($this->httpClient, $this->requestBuilder, $this->hydrator);
	}

	public function callAlerts(): CallAlerts
	{
		return new CallAlerts($this->httpClient, $this->requestBuilder, $this->hydrator);
	}

	public function callComments(): CallComments
	{
		return new CallComments($this->httpClient, $this->requestBuilder, $this->hydrator);
	}

	public function calls(): Calls
	{
		return new Calls($this->httpClient, $this->requestBuilder, $this->hydrator);
	}

    public function me(): Me
    {
        return new Me($this->httpClient, $this->requestBuilder, $this->hydrator);
    }

    public function periods(): Periods
    {
        return new Periods($this->httpClient, $this->requestBuilder, $this->hydrator);
    }

    public function volunteers(): Volunteers
    {
        return new Volunteers($this->httpClient, $this->requestBuilder, $this->hydrator);
    }

	public function schedules(): Schedules
	{
		return new Schedules($this->httpClient, $this->requestBuilder, $this->hydrator);
	}
}
