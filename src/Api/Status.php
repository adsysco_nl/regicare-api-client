<?php

namespace Adsysco\RegiCareApiClient\Api;

use Psr\Http\Message\ResponseInterface;
use Adsysco\RegiCareApiClient\Models\Status as StatusModel;

class Status extends HttpApi
{
	/**
	 * Retrieve the status.
	 *
	 * @return mixed|ResponseInterface
	 */
	public function get()
	{
		$response = $this->httpGet('api/v2/status');

		return $this->hydrateResponse($response, StatusModel::class);
	}
}
