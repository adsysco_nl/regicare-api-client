<?php

namespace Adsysco\RegiCareApiClient\Api\RegiVrijwilliger;

use Adsysco\RegiCareApiClient\Api\HttpApi;
use Adsysco\RegiCareApiClient\Models\RegiVrijwilliger\Organisation;

class Organisations extends HttpApi
{
  /**
   * Retrieve all organisations from the API.
   *
   * @return mixed|ResponseInterface
   */
  public function all()
  {
    $response = $this->httpGet('api/v2/regivrijwilliger/organisations');

    return $this->hydrateResponseCollection($response, Organisation::class);
  }
}
