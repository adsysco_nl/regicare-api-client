<?php

namespace Adsysco\RegiCareApiClient\Api;

use Adsysco\RegiCareApiClient\Api\RegiVrijwilliger\Organisations;

class RegiVrijwilliger extends HttpApi
{
	/**
	 * Access the ‘organisations’ namespace.
	 *
	 * @return void
	 */
	public function organisations(): Organisations
	{
		return new Organisations($this->httpClient, $this->requestBuilder, $this->hydrator);
	}
}
