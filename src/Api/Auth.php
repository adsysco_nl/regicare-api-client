<?php

namespace Adsysco\RegiCareApiClient\Api;

use Adsysco\RegiCareApiClient\Client\Auth\Token;
use Adsysco\RegiCareApiClient\Client\Credentials\OAuthClientCredentials;
use Adsysco\RegiCareApiClient\Client\Credentials\OAuthPasswordGrantCredentials;

class Auth extends HttpApi
{
	public function authenticateWithPasswordGrant(OAuthPasswordGrantCredentials $credentials)
	{
		$response = $this->httpPost('api/v2/oauth/token', $credentials->toArray());
		return $this->hydrateResponse($response, Token::class);
	}

	public function authenticateWithClientCredentialsGrant(OAuthClientCredentials $credentials)
	{
		$response = $this->httpPost('api/v2/oauth/token', $credentials->toArray());
		return $this->hydrateResponse($response, Token::class);
	}
}