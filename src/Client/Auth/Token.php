<?php

namespace Adsysco\RegiCareApiClient\Client\Auth;

use Adsysco\RegiCareApiClient\Models\ApiResponse;

class Token implements ApiResponse
{
	private $token;

	public function __construct($token)
	{
		$this->token = $token;
	}

	/**
	 * Create an API response object from the HTTP response from the API server.
	 *
	 * @param array $data
	 *
	 * @return Token
	 */
	public static function create(array $data): self
	{
		return new self($data);
	}

	public function getAccessToken()
	{
		return $this->token['access_token'];
	}
}
