<?php

namespace Adsysco\RegiCareApiClient\Client\Credentials;

class OAuthPasswordGrantCredentials
{
	private $clientId;
	private $clientSecret;
	private $username;
	private $password;

	public function __construct($clientId, $clientSecret, $username, $password)
	{
		$this->clientId = $clientId;
		$this->clientSecret = $clientSecret;
		$this->username = $username;
		$this->password = $password;
	}

	public function toArray()
	{
		return [
			'grant_type' => 'password',
			'client_id' => (string) $this->clientId,
			'client_secret' => $this->clientSecret,
			'username' => $this->username,
			'password' => $this->password,
		];
	}
}
