<?php

namespace Adsysco\RegiCareApiClient\Client\Credentials;

class OAuthClientCredentials
{
	private $clientId;
	private $clientSecret;

	public function __construct($clientId, $clientSecret)
	{
		$this->clientId = $clientId;
		$this->clientSecret = $clientSecret;
	}

	public function toArray()
	{
		return [
			'grant_type' => 'client_credentials',
			'client_id' => (string) $this->clientId,
			'client_secret' => $this->clientSecret,
			'scope' => ''
		];
	}
}
