<?php

namespace Adsysco\RegiCareApiClient\Models;

interface ApiResponse
{
	/**
	 * Create an API response object from the HTTP response from the API server.
	 *
	 * @param array $data
	 */
	public static function create(array $data);
}