<?php

namespace Adsysco\RegiCareApiClient\Models\RegiCall;

use Carbon\CarbonInterval;
use Adsysco\RegiCareApiClient\Models\Model;

class CallAttempt extends Model
{
    const VOLUNTEER_QUEUED = 'volunteer-queued';
    const VOLUNTEER_INITIATED = 'volunteer-initiated';
    const VOLUNTEER_RINGING = 'volunteer-ringing';
    const VOLUNTEER_IN_PROGRESS = 'volunteer-in-progress';
    const VOLUNTEER_COMPLETED = 'volunteer-completed';
    const VOLUNTEER_BUSY = 'volunteer-busy';
    const VOLUNTEER_CANCELED = 'volunteer-canceled';
    const VOLUNTEER_FAILED = 'volunteer-failed';
    const VOLUNTEER_NO_ANSWER = 'volunteer-no-answer';

    const CLIENT_COMPLETED = 'client-completed';
    const CLIENT_BUSY = 'client-busy';
    const CLIENT_CANCELED = 'client-canceled';
    const CLIENT_FAILED = 'client-failed';
    const CLIENT_NO_ANSWER = 'client-no-answer';

	/**
	 * @return CarbonInterval
	 */
	public function duration(): CarbonInterval
	{
		return CarbonInterval::seconds($this->durationInSeconds());
	}

	/**
	 * @return int
	 */
	public function durationInSeconds(): int
	{
		if (!$this->startAndEndAreSet()) {
			return 0;
		}

		return $this->duration_in_seconds;
	}

	/**
	 * @return bool
	 */
	private function startAndEndAreSet(): bool
	{
		return $this->start && $this->end;
	}
}
