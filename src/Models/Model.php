<?php

namespace Adsysco\RegiCareApiClient\Models;

abstract class Model implements ApiResponse
{
	protected $attributes;

	protected $exists;

	public function __construct($data)
	{
		$this->attributes = $data;
	}


	public static function create(array $data)
	{
		return new static($data);
	}

	public function toArray()
	{
	    return $this->attributes;
	}

	public function toJson()
	{
		return json_encode($this->toArray());
	}

	public function __get($param)
	{
		return $this->attributes[$param];
	}

	// @todo - remove me after DEBUG
	public function __set($param, $value)
	{
		return $this->attributes[$param] = $value;
	}
}