<?php

namespace Adsysco\RegiCareApiClient\Rest;

use Adsysco\RegiCareApiClient\Api\Auth;
use Adsysco\RegiCareApiClient\Api\RegiCall;
use Adsysco\RegiCareApiClient\Api\RegiVrijwilliger;
use Adsysco\RegiCareApiClient\Api\Status;
use Adsysco\RegiCareApiClient\Client\Auth\Token;
use Adsysco\RegiCareApiClient\Client\Credentials\OAuthClientCredentials;
use Adsysco\RegiCareApiClient\Client\Credentials\OAuthPasswordGrantCredentials;
use Adsysco\RegiCareApiClient\Contracts\RegiCareApiClientInterface;
use Adsysco\RegiCareApiClient\HttpClient\HttpClientConfigurator;
use Adsysco\RegiCareApiClient\HttpClient\RequestBuilder;
use Adsysco\RegiCareApiClient\Hydrator\Hydrator;
use Adsysco\RegiCareApiClient\Hydrator\ModelHydrator;
use Adsysco\RegiCareApiClient\Models\Status as StatusModel;
use Http\Client\Common\PluginClient;
use Psr\Http\Client\ClientInterface;

class Client implements RegiCareApiClientInterface
{
    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var integer
     */
    protected $clientID;

    /**
     * @var string
     */
    protected $clientSecret;

    /**
     * @var RequestBuilder
     */
    protected $requestBuilder;

    /**
     * @var ClientInterface|PluginClient
     */
    private $httpClient;

    /**
     * @var Hydrator
     */
    private $hydrator;

    /**
     * Construct a Client instance.
     *
     * @param HttpClientConfigurator $configurator
     * @param mixed Hydrator
     * @param mixed RequestBuilder
     * @return void
     */
    public function __construct(HttpClientConfigurator $configurator, Hydrator $hydrator = null, RequestBuilder $requestBuilder = null )
    {
        $this->requestBuilder = $requestBuilder ?: new RequestBuilder();
        $this->httpClient = $configurator->createConfiguredClient();
        $this->endpoint = $configurator->getEndpoint();
        $this->hydrator = $hydrator ?: new ModelHydrator();
    }

    /**
     * Get the baseUrl of the API.
     *
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * Create a client instance for a specific endpoint.
     * 
     * @param  string  $endpoint
     * @param  bool  $debug
     * @return Client
     */
    public static function create(string $endpoint, bool $debug = false): self
    {
        $httpClientConfigurator = (new HttpClientConfigurator())
            ->setDebug($debug)
            ->setEndpoint($endpoint);
        return new self($httpClientConfigurator);
    }

    /**
     * Create an authenticated Client instance.
     *
     * @param Token $token
     * @param string $endpoint
     * @param bool $debug
     * @return Client
     */
    public static function withAuthenticationToken(Token $token, string $endpoint = 'http://regiweb2.localhost', bool $debug = false): self
    {
        $httpClientConfigurator = (new HttpClientConfigurator())
            ->setDebug($debug)
            ->setEndpoint($endpoint)
            ->setToken($token);
        return new self($httpClientConfigurator);
    }

    public static function generateTokenWithPasswordGrant(OAuthPasswordGrantCredentials $credentials, string $endpoint = 'http://regiweb2.localhost', bool $debug = false): Token
    {
        $httpClientConfigurator = (new HttpClientConfigurator())
            ->setDebug($debug)
            ->setEndpoint($endpoint);
        $client = new self($httpClientConfigurator);
        return $client->authenticateWithPasswordGrant($credentials);
    }

    public function generateTokenWithClientCredentialsGrant(OAuthClientCredentials $credentials, string $endpoint = 'http://regiweb.localhost', bool $debug = false): Token
    {
        $httpClientConfigurator = (new HttpClientConfigurator())
            ->setDebug($debug)
            ->setEndpoint($endpoint);
        $client = new self($httpClientConfigurator);
        return $client->authenticateWithClientCredentialsGrant($credentials);
    }

    public function authenticateWithPasswordGrant(OAuthPasswordGrantCredentials $credentials): Token
    {
        return (new Auth($this->httpClient, $this->requestBuilder, $this->hydrator))->authenticateWithPasswordGrant($credentials);
    }

    public function authenticateWithClientCredentialsGrant(OAuthClientCredentials $credentials): Token
    {
        return (new Auth($this->httpClient, $this->requestBuilder, $this->hydrator))->authenticateWithClientCredentialsGrant($credentials);
    }

    /**
     * Access the ‘status’ namespace.
     *
     * @return StatusModel
     */
    public function status(): StatusModel
    {
        return (new Status($this->httpClient, $this->requestBuilder, $this->hydrator))->get();
    }

    /**
     * Access the ‘RegiCall’ namespace.
     *
     * @return RegiCall
     */
    public function regiCall(): RegiCall
    {
        return new RegiCall($this->httpClient, $this->requestBuilder, $this->hydrator);
    }

    /**
     * Access the ‘RegiVrijwilliger’ namespace.
     *
     * @return RegiVrijwilliger
     */
    public function regiVrijwilliger(): RegiVrijwilliger
    {
        return new RegiVrijwilliger($this->httpClient, $this->requestBuilder, $this->hydrator);
    }
}
