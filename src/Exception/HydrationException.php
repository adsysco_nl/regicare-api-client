<?php

namespace Adsysco\RegiCareApiClient\Exception;

use Adsysco\RegiCareApiClient\Exception;

final class HydrationException extends \RuntimeException implements Exception
{
}
