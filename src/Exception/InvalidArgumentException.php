<?php

namespace Adsysco\RegiCareApiClient\Exception;

use Adsysco\RegiCareApiClient\Exception;

final class InvalidArgumentException extends \InvalidArgumentException implements Exception
{
}
