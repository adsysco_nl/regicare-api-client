<?php

namespace Adsysco\RegiCareApiClient\Hydrator;

use Psr\Http\Message\ResponseInterface;
use Adsysco\RegiCareApiClient\Models\ApiResponse;
use Adsysco\RegiCareApiClient\Exception\HydrationException;

/**
 * Serialize an HTTP response to domain object.
 */
final class ModelHydrator implements Hydrator
{
	/**
	 * @return ResponseInterface
	 */
	public function hydrate(ResponseInterface $response, string $class)
	{
		$data = $this->parseDataFromResponse($response);

		return $this->createObject($data, $class);
	}

	public function hydrateCollection(ResponseInterface $response, string $class)
	{
		$data = $this->parseDataFromResponse($response);
		return array_map(function($item) use ($class) {
			return $this->createObject($item, $class);
		}, $data);
	}

	private function createObject($data, $class)
	{
		if (is_subclass_of($class, ApiResponse::class)) {
			$object = call_user_func($class.'::create', $data);
		} else {
			$object = new $class($data);
		}

		return $object;
	}

	/**
	 * @param ResponseInterface $response
	 *
	 * @return array
	 */
	protected function parseDataFromResponse(ResponseInterface $response): array
	{
		$body = $response->getBody()->__toString();
		$contentType = $response->getHeaderLine('Content-Type');

		if (0 !== strpos($contentType, 'application/json')
		    && 0 !== strpos($contentType, 'application/octet-stream')) {
			throw new HydrationException('The ModelHydrator cannot hydrate response with Content-Type: '
			                             . $contentType);
		}

		$data = json_decode($body, true);

		if (JSON_ERROR_NONE !== json_last_error()) {
			throw new HydrationException(sprintf('Error (%d) when trying to json_decode response',
				json_last_error()));
		}

		return $data;
}
}