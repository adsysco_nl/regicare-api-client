<?php

declare(strict_types=1);

/*
 * Copyright (C) 2013 Mailgun
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace Adsysco\RegiCareApiClient\Hydrator;

use Psr\Http\Message\ResponseInterface;
use Adsysco\RegiCareApiClient\Exception\HydrationException;

/**
 * Deserialize a PSR-7 response to something else.
 */
interface Hydrator
{
	/**
	 * @throws HydrationException
	 */
	public function hydrate(ResponseInterface $response, string $class);

	/**
	 * @throws HydrationException
	 */
	public function hydrateCollection(ResponseInterface $response, string $class);
}