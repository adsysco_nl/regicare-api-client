<?php

namespace Adsysco\RegiCareApiClient\Hydrator;

use Psr\Http\Message\ResponseInterface;

/**
 * Do not serialize at all. Just return a PSR-7 response.
 *
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
final class NoopHydrator implements Hydrator
{
	/**
	 * @throws \LogicException
	 */
	public function hydrate(ResponseInterface $response, string $class)
	{
		throw new \LogicException('The NoopHydrator should never be called');
	}
}