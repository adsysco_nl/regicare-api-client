<?php

namespace Adsysco\RegiCareApiClient\HttpClient;

use Adsysco\RegiCareApiClient\Client\Auth\Token;
use Http\Client\Common\Plugin;
use Http\Client\Common\Plugin\AuthenticationPlugin;
use Http\Client\Common\Plugin\LoggerPlugin;
use Http\Client\Common\PluginClient;
use Http\Discovery\Psr17FactoryDiscovery;
use Http\Discovery\Psr18ClientDiscovery;
use Monolog\Logger;
use Psr\Http\Client\ClientInterface;
use Http\Message\Authentication\Bearer;
use Psr\Http\Message\UriFactoryInterface;
use Http\Message\Formatter\FullHttpMessageFormatter;

/**
 * Configure a HTTP client.
 *
 */
final class HttpClientConfigurator
{
    /**
     * @var string
     */
    private $endpoint;

    /**
     * If debug is true we will send all the request to the endpoint without appending any path.
     *
     * @var bool
     */
    private $debug = false;

	/**
	 * @var Token
	 */
	private $token;

    /**
     * @var UriFactoryInterface
     */
    private $uriFactory;

    /**
     * @var ClientInterface
     */
    private $httpClient;

    public function createConfiguredClient(): PluginClient
    {
	    $plugins = [
		    new Plugin\BaseUriPlugin($this->getUriFactory()->createUri($this->endpoint)),
		    new Plugin\HeaderDefaultsPlugin([
                'User-Agent' => 'regicare-sdk-php/v1 (https://github.com/adsysco/regicare-api-client)',
            ]),
        ];

	    if ($this->token) {
	    	$plugins[] = new AuthenticationPlugin(new Bearer($this->token->getAccessToken()));
	    }

        if ($this->debug) {
            $plugins[] =  new LoggerPlugin(new Logger('http'), new FullHttpMessageFormatter());
        }

        return new PluginClient($this->getHttpClient(), $plugins);
    }

    public function setDebug(bool $debug): self
    {
        $this->debug = $debug;

        return $this;
    }

    public function setEndpoint(string $endpoint): self
    {
        $this->endpoint = $endpoint;

        return $this;
    }

    private function getUriFactory(): UriFactoryInterface
    {
        if (null === $this->uriFactory) {
            $this->uriFactory = Psr17FactoryDiscovery::findUrlFactory();
        }

        return $this->uriFactory;
    }

    public function setUriFactory(UriFactoryInterface $uriFactory): self
    {
        $this->uriFactory = $uriFactory;

        return $this;
    }

    private function getHttpClient(): ClientInterface
    {
        if (null === $this->httpClient) {
            $this->httpClient = Psr18ClientDiscovery::find();
        }

        return $this->httpClient;
    }

    public function setHttpClient(ClientInterface $httpClient): self
    {
        $this->httpClient = $httpClient;

        return $this;
    }

	/**
	 * @return Token
	 */
	public function getToken(): Token
	{
		return $this->token;
	}

	/**
	 * @param Token $token
	 *
	 * @return HttpClientConfigurator
	 */
	public function setToken(Token $token): self
	{
		$this->token = $token;

		return $this;
    }
    
    public function getEndpoint()
    {
        return $this->endpoint;
    }
}
