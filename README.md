# RegiCare API Client

[![Latest Version on Packagist](https://img.shields.io/packagist/v/adsysco/regicare-api-client.svg?style=flat-square)](https://packagist.org/packages/adsysco/regicare-api-client)
[![Build Status](https://img.shields.io/travis/adsysco/regicare-api-client/master.svg?style=flat-square)](https://travis-ci.org/adsysco/regicare-api-client)
[![Quality Score](https://img.shields.io/scrutinizer/g/adsysco/regicare-api-client.svg?style=flat-square)](https://scrutinizer-ci.com/g/adsysco/regicare-api-client)
[![Total Downloads](https://img.shields.io/packagist/dt/adsysco/regicare-api-client.svg?style=flat-square)](https://packagist.org/packages/adsysco/regicare-api-client)

Provides an easy to use API client around the RegiCare API v2.

## Installation

You can install the package via composer:

```bash
composer require adsysco/regicare-api-client
```

## Usage

``` php
use namespace Adsysco\RegiCareApiClient\Rest\Client;

// First create an access token.
$credentials = new OAuthPasswordGrantCredentials(2, '5VGaw6IVkK6dVLUAV6nHBs0JbgBsT6JBja3jy0V2', 'valid-user@adsysco.nl', 'valid-password');
$token = Client::generateTokenWithPasswordGrant($credentials);

// Then create a client that authenticates with that token.
$client = Client::withAuthenticationToken($token);

// Get Call with ID 1:
$call = $client->regiCall()->calls()->show(1);
```

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email tvandenanker@adsycso.nl instead of using the issue tracker.

## Credits

- [Thijs van den Anker](https://github.com/adsysco)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## PHP Package Boilerplate

This package was generated using the [PHP Package Boilerplate](https://laravelpackageboilerplate.com).