<?php

namespace Adsysco\RegiCareApiClient\Tests;

use Adsysco\RegiCareApiClient\Client\Credentials\OAuthPasswordGrantCredentials;
use Adsysco\RegiCareApiClient\Tests\Traits\hasAuthenticatedClient;
use PHPUnit\Framework\TestCase;
use Adsysco\RegiCareApiClient\Rest\Client;

class SmokeTest extends TestCase
{
    /** @test */
    public function smoke_test_the_api_client()
    {
    	$this->endpoint = 'http://localhost:8000';
        $this->assertTrue(true);
//	    $api = Client::create(1, 'foo', 'valid-user@adsysco.nl', 'valid-password', 'http://localhost:8002');
	    $credentials = new OAuthPasswordGrantCredentials(2, '5VGaw6IVkK6dVLUAV6nHBs0JbgBsT6JBja3jy0V2', 'valid-user@adsysco.nl', 'valid-password');
	    $token = Client::generateTokenWithPasswordGrant($credentials, $endpoint);
	    $this->assertNotEmpty($token->getAccessToken());

	    $client = Client::withAuthenticationToken($token, $endpoint);
        $call = $client->regiCall()->calls()->show(1);
        $this->assertEquals(1, $call->id);

        $notes = $client->regiCall()->notes()->index(['user_id' => 1]);
        die(var_dump($notes));
    }
}
