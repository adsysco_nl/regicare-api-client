<?php

namespace Adsysco\RegiCareApiClient\Tests\Models\RegiCall;

use Adsysco\RegiCareApiClient\Tests\TestCase;

class CallAttemptTest extends TestCase
{
	/** @test */
	public function it_can_calculate_the_duration()
	{
		$callAttempt = $this->client()->regiCall()->callAttempts()->show(1);

		$this->assertGreaterThan(0, $callAttempt->duration()->seconds);
		$this->assertEquals($callAttempt->duration()->seconds, $callAttempt->durationInSeconds());
	}
}
