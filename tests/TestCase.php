<?php

namespace Adsysco\RegiCareApiClient\Tests;

use Adsysco\RegiCareApiClient\Rest\Client;
use PHPUnit\Framework\TestCase as BaseTestCase;
use Adsysco\RegiCareApiClient\Client\Credentials\OAuthPasswordGrantCredentials;

class TestCase extends BaseTestCase
{
	/**
	 * @var string
	 */
	protected $endpoint = 'http://localhost:8000';

	/**
	 * @var Client
	 */
	protected $client;

	public function client()
	{
		if ($this->client === null)
		{
			$credentials = new OAuthPasswordGrantCredentials(2, '5VGaw6IVkK6dVLUAV6nHBs0JbgBsT6JBja3jy0V2', 'valid-user@adsysco.nl', 'valid-password');
			$token = Client::generateTokenWithPasswordGrant($credentials, $this->endpoint);

			$this->client = Client::withAuthenticationToken($token, $this->endpoint);
		}

		return $this->client;
	}

	public function dump($data)
	{
		var_dump($data);
	}
}