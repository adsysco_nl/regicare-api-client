<?php

namespace Adsysco\RegiCareApiClient\Tests\Api\RegiCall;

use Adsysco\RegiCareApiClient\Tests\TestCase;
use Adsysco\RegiCareApiClient\Models\RegiCall\Call;

class CallsTest extends TestCase
{
	/** @test */
	public function it_has_an_index_method()
	{
		$result = $this->client()->regiCall()->calls()->index();

		$this->assertIsArray($result);
	}

	/** @test */
	public function it_has_a_show_method()
	{
		$result = $this->client()->regiCall()->calls()->show(1);

		$this->assertInstanceOf(Call::class, $result);
	}
}
