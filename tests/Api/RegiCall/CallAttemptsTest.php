<?php

namespace Adsysco\RegiCareApiClient\Tests\Api\RegiCall;

use Adsysco\RegiCareApiClient\Tests\TestCase;
use Adsysco\RegiCareApiClient\Models\RegiCall\CallAttempt;

class CallAttemptsTest extends TestCase
{
	/** @test */
	public function it_has_an_index_method()
	{
		$result = $this->client()->regiCall()->callAttempts()->index(['call_id' => 1]);

		$this->assertIsArray($result);
	}

	/** @test */
	public function it_has_a_show_method()
	{
		$result = $this->client()->regiCall()->callAttempts()->show(1);

		$this->assertInstanceOf(CallAttempt::class, $result);
	}

	/** @test */
	public function it_has_a_create_method()
	{
		$callId = 1;
		$result = $this->client()->regiCall()->callAttempts()->create($callId);

		$this->assertInstanceOf(CallAttempt::class, $result);
	}
}
